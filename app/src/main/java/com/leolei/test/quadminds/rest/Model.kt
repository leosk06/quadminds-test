package com.leolei.test.quadminds.rest

import com.google.gson.annotations.SerializedName
import com.leolei.test.quadminds.model.Note

abstract class Response {
    @SerializedName("message")
    var errorMessage: String? = null
}

class NotesResponse : Response() {
    var data: List<Note>? = null
}

class NoteResponse : Response() {
    var data: Note? = null
}

class DeleteResponse : Response() {
    var data: Boolean = false
}


