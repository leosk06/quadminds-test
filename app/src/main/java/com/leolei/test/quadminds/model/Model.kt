package com.leolei.test.quadminds.model

data class Note(
        val id: String,
        val title: String,
        val content: String
)

data class NotePost(
        val title: String,
        val content: String
)

data class NotePatch(
        val title: String,
        val content: String
)

