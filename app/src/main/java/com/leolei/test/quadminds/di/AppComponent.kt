package com.leolei.test.quadminds.di

import com.leolei.test.quadminds.App
import com.leolei.test.quadminds.notes.NotesActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun app(): App

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        @BindsInstance
        fun baseUrl(url: String): Builder

        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(target: NotesActivity)
}
