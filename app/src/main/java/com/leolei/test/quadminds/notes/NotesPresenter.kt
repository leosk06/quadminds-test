package com.leolei.test.quadminds.notes

import com.leolei.test.quadminds.mvp.MvpPresenter

interface NotesPresenter: MvpPresenter<NotesView> {
    fun loadNotes()
    fun errorDismissed()
    fun editNote(id: String, title: String, content: String)
    fun createNote(title: String, content: String)
    fun deleteNotes(ids: List<String>)
}
