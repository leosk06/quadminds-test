package com.leolei.test.quadminds.notes

import android.os.Bundle
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.leolei.test.quadminds.App
import com.leolei.test.quadminds.R
import com.leolei.test.quadminds.model.Note
import com.leolei.test.quadminds.notes.NotesAdapter.EditionListener
import com.leolei.test.quadminds.util.SimpleActionModeCallback
import kotlinx.android.synthetic.main.activity_notes.*
import javax.inject.Inject

class NotesActivity : AppCompatActivity(), NotesView, EditionListener {
    @Inject
    lateinit var presenter: NotesPresenter

    private lateinit var adapter: NotesAdapter

    private var errorBar: Snackbar? = null

    private var editActionMode: ActionMode? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)

        App.component.inject(this)

        setupRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when (item.itemId) {
            R.id.add -> {
                adapter.addNewNote()
                list.post {
                    list.scrollToPosition(adapter.itemCount - 1)
                }
            }
            R.id.remove -> {
                adapter.startChoiceMode()
                startSupportActionMode(object : SimpleActionModeCallback() {
                    override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                        menuInflater.inflate(R.menu.action_mode_remove, menu)
                        return true
                    }

                    override fun onActionItemClicked(actionMode: ActionMode, item: MenuItem): Boolean {
                        val chosenItems = adapter.choiceModeItems.map { it } // new list to avoid race conditions
                        presenter.deleteNotes(chosenItems)
                        actionMode.finish()
                        return true
                    }

                    override fun onDestroyActionMode(actionMode: ActionMode) {
                        adapter.finishChoiceMode()
                    }
                })
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        presenter.setView(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.setView(null)
        errorBar = null
    }

    private fun setupRecyclerView() {
        list.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        adapter = NotesAdapter(this)
        adapter.editionListener = this
        list.adapter = adapter
    }

    override fun onNoteEdited(id: String, title: String, content: String) {
        editActionMode?.finish()

        val isNewNote = id.isEmpty()
        if (isNewNote) {
            presenter.createNote(title, content)
        } else {
            presenter.editNote(id, title, content)
        }
    }

    override fun onEditModeStarted() {
        editActionMode = startSupportActionMode(object : SimpleActionModeCallback() {
            override fun onDestroyActionMode(actionMode: ActionMode) {
                super.onDestroyActionMode(actionMode)
                adapter.finishEditMode()
            }
        })
    }

    override fun showNotes(notes: List<Note>?) {
        notes?.let {
            adapter.data = notes
        }
    }

    override fun showLoading(loading: Boolean) {
        loadingOverlay.visibility = if (loading) View.VISIBLE else View.GONE
    }

    override fun showError(errorMessage: String?) {
        if (errorMessage == null) {
            errorBar?.dismiss()
            return
        }

        errorBar = Snackbar.make(
                root,
                errorMessage,
                Snackbar.LENGTH_INDEFINITE
        )

        errorBar?.let { bar ->
            bar.setAction(R.string.dismiss) {}
            bar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    when (event) {
                        DISMISS_EVENT_SWIPE,
                        DISMISS_EVENT_ACTION -> presenter.errorDismissed()
                    }
                }
            })
            bar.show()
        }
    }
}
