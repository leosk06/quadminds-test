package com.leolei.test.quadminds.notes

import android.content.Context
import android.support.annotation.IntDef
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.leolei.test.quadminds.R
import com.leolei.test.quadminds.model.Note
import java.util.*

class NotesAdapter(context: Context) : RecyclerView.Adapter<NoteVH>() {
    companion object {
        const val POSITION_NONE = -1

        const val TYPE_READ_ONLY = 0
        const val TYPE_EDITABLE = 1

        @IntDef(TYPE_READ_ONLY, TYPE_EDITABLE)
        annotation class ViewType
    }

    interface EditionListener {
        fun onNoteEdited(id: String, title: String, content: String)
        fun onEditModeStarted()
    }

    var editionListener: EditionListener? = null

    private val inflater = LayoutInflater.from(context)

    var data: List<Note> = Collections.emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private var editingIndex = POSITION_NONE
        set(value) {
            if (value == field) {
                return
            }
            val oldValue = field
            field = value
            if (oldValue >= 0) {
                notifyItemChanged(oldValue)
            }
            if (value >= 0) {
                notifyItemChanged(value)
                editionListener?.onEditModeStarted()
            }
        }

    private var choiceMode = false
        set(value) {
            if (field == value) {
                return
            }

            field = value

            if (value) {
                finishEditMode()
            }
            notifyDataSetChanged()
        }

    val choiceModeItems = ArrayList<String>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteVH {
        val isEditable = viewType == TYPE_EDITABLE
        val layoutId = if (isEditable) R.layout.item_note_editable else R.layout.item_note_read_only
        val itemView = inflater.inflate(layoutId, parent, false)
        val holder = if (isEditable) EditableNoteVH(itemView) else NoteVH(itemView)

        holder.edit.setOnClickListener {
            when (editingIndex) {
                POSITION_NONE -> startEditing(holder)
                holder.boundPosition -> finishEditing(holder)
            }
        }

        holder.choose?.setOnCheckedChangeListener { _, isChecked ->
            val itemId = data[holder.boundPosition].id
            if (isChecked) {
                choiceModeItems.add(itemId)
            } else {
                choiceModeItems.remove(itemId)
            }
        }
        return holder
    }

    override fun getItemCount() = data.size

    @ViewType
    override fun getItemViewType(position: Int): Int {
        return if (position == editingIndex) TYPE_EDITABLE else TYPE_READ_ONLY
    }

    override fun onBindViewHolder(holder: NoteVH, position: Int) {
        val note = data[position]
        with(holder) {
            boundPosition = position

            title.text = note.title
            content.text = note.content

            if (this is EditableNoteVH) {
                title.setSelection(title.text.length)
                content.setSelection(content.text.length)
            }

            if (choiceMode) {
                choose?.visibility = View.VISIBLE
                edit.visibility = View.GONE
            } else {
                choose?.visibility = View.GONE
                choose?.isChecked = false
                edit.visibility = View.VISIBLE
            }
        }
    }

    private fun startEditing(holder: NoteVH) {
        editingIndex = holder.boundPosition
    }

    private fun finishEditing(holder: NoteVH) {
        val note = data[editingIndex]
        editionListener?.onNoteEdited(
                note.id,
                holder.title.text.toString(),
                holder.content.text.toString()
        )
        finishEditMode()
    }


    fun addNewNote() {
        if (editingIndex == POSITION_NONE) {
            val newData = ArrayList<Note>(data)
            data = newData
            val newItemIndex = data.size
            newData.add(Note("", "", ""))
            notifyItemInserted(newItemIndex)
            editingIndex = newItemIndex
        }
    }

    private fun undoAddNewNote() {
        val lastIndex = data.lastIndex
        val hasNewNote = data[lastIndex].id == ""
        if (hasNewNote) {
            // data is List<>, so it might not have remove() methods
            val newData = ArrayList<Note>(data)
            newData.removeAt(lastIndex)
            data = newData
            notifyItemRemoved(lastIndex)
        }
    }

    fun finishEditMode() {
        editingIndex = POSITION_NONE
        undoAddNewNote()
    }

    fun startChoiceMode() {
        choiceMode = true
    }

    fun finishChoiceMode() {
        choiceMode = false
        choiceModeItems.clear()
    }
}

open class NoteVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    open val title: TextView = itemView.findViewById(R.id.title)
    open val content: TextView = itemView.findViewById(R.id.content)
    val choose: CheckBox? = itemView.findViewById(R.id.choose)
    val edit: View = itemView.findViewById(R.id.edit)
    var boundPosition = -1
}

class EditableNoteVH(itemView: View) : NoteVH(itemView) {
    override val title: EditText = super.title as EditText
    override val content: EditText = super.content as EditText
}