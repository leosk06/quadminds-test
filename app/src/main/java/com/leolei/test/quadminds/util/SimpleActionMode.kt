package com.leolei.test.quadminds.util

import android.support.v7.view.ActionMode
import android.view.Menu
import android.view.MenuItem

open class SimpleActionModeCallback : ActionMode.Callback {
    override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean {
        return true
    }

    override fun onPrepareActionMode(actionMode: ActionMode, menu: Menu): Boolean {
        return false
    }

    override fun onActionItemClicked(actionMode: ActionMode, item: MenuItem): Boolean {
        return false
    }

    override fun onDestroyActionMode(actionMode: ActionMode) {
    }
}