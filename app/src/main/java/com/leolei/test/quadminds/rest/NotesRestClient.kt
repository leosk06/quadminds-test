package com.leolei.test.quadminds.rest

import com.leolei.test.quadminds.model.NotePatch
import com.leolei.test.quadminds.model.NotePost
import io.reactivex.Single
import retrofit2.http.*

interface NotesRestClient {
	@GET("notes")
	fun getNotes(): Single<NotesResponse>

	@POST("notes")
	fun createNote(@Body note: NotePost): Single<NoteResponse>

	@GET("notes/{noteId}")
	fun getNote(@Path("noteId") noteId: String): Single<NoteResponse>

	@PUT("notes/{noteId}")
	fun updateNote(@Path("noteId") noteId: String, @Body update: NotePost): Single<NoteResponse>

	@PATCH("notes/{noteId}")
	fun partiallyUpdateNote(@Path("noteId") noteId: String, @Body update: NotePatch): Single<NoteResponse>

	@DELETE("notes/{noteId}")
	fun deleteNote(@Path("noteId") noteId: String): Single<DeleteResponse>
}

