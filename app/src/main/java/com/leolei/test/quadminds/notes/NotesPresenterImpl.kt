package com.leolei.test.quadminds.notes

import com.leolei.test.quadminds.model.Note
import com.leolei.test.quadminds.model.NotePost
import com.leolei.test.quadminds.rest.NotesRestClient
import com.leolei.test.quadminds.rest.Response
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class NotesPresenterImpl(
        private val restClient: NotesRestClient
) : NotesPresenter {
    private var loading = false
    private val notes: ArrayList<Note> = ArrayList()
    private var error: String? = null
    private var view: NotesView? = null

    override fun setView(view: NotesView?) {
        this.view = view
        view?.let { newView ->
            newView.showLoading(loading)
            newView.showNotes(notes)
            newView.showError(error)
        }

        loadNotes()
    }

    override fun loadNotes() {
        view?.showLoading(true)
        restClient.getNotes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            if (!notifyViewIfErrorResponse(response)) {
                                val newNotes = response.data ?: Collections.emptyList()
                                updateNotes(newNotes)
                                view?.showLoading(false)
                            }
                        },
                        ::notifyError
                )
    }

    override fun editNote(id: String, title: String, content: String) {
        startLoading()
        restClient.updateNote(id, NotePost(title, content))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            if (!notifyViewIfErrorResponse(response)) {
                                response.data?.let { newNote ->
                                    val index = notes.indexOfFirst { it.id == id }
                                    val newNotes = ArrayList<Note>(notes)
                                    newNotes[index] = newNote
                                    updateNotes(newNotes)
                                    view?.showLoading(false)
                                }
                            }
                        },
                        ::notifyError
                )
    }

    override fun createNote(title: String, content: String) {
        startLoading()
        restClient.createNote(NotePost(title, content))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            if (!notifyViewIfErrorResponse(response)) {
                                response.data?.let { newNote ->
                                    val newNotes = ArrayList<Note>(notes)
                                    newNotes.add(newNote)
                                    updateNotes(newNotes)
                                    view?.showLoading(false)
                                }
                            }
                        },
                        ::notifyError
                )
    }

    override fun deleteNotes(ids: List<String>) {
        startLoading()

        Observable.fromIterable(ids)
                .flatMap { id ->
                    restClient.deleteNote(id)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                                .map { response -> Pair(id, response) }
                            .toObservable()
                }
                .subscribe(
                        { (id, response) ->
                            if (!notifyViewIfErrorResponse(response)) {
                                if (response.data) {
                                    val newNotes = ArrayList<Note>(notes)
                                    newNotes.removeAt(newNotes.indexOfFirst { it.id == id })
                                    updateNotes(newNotes)
                                }
                            }
                        },
                        ::notifyError,
                        {
                            view?.showLoading(false)
                        }
                )
    }

    private fun updateNotes(newNotes: List<Note>) {
        notes.clear()
        notes.addAll(newNotes)
        view?.showNotes(notes.map { it.copy() })
    }


    private fun startLoading() {
        view?.let { view ->
            view.showLoading(true)
            view.showError(null)
        }
    }

    private fun notifyViewIfErrorResponse(response: Response): Boolean {
        response.errorMessage?.let { errorMsg ->
            if (response.errorMessage != null) {
                notifyError(errorMsg)
                return true
            }
        }
        return false
    }

    private fun notifyError(msg: String?) {
        view?.let { view ->
            view.showLoading(false)
            if (msg != null) {
                view.showError(msg)
            }
        }
    }

    private fun notifyError(error: Throwable) {
        notifyError(error.message)
    }

    override fun errorDismissed() {
        error = null
    }
}
