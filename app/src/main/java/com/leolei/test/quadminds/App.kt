package com.leolei.test.quadminds

import android.support.multidex.MultiDexApplication
import com.leolei.test.quadminds.di.AppComponent
import com.leolei.test.quadminds.di.DaggerAppComponent

class App : MultiDexApplication() {
    companion object {
        lateinit var instance: App private set
        lateinit var component: AppComponent private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        component = DaggerAppComponent.builder()
                .app(this)
                .baseUrl("https://quadminds-notes-test.getsandbox.com/")
                .build()

    }
}