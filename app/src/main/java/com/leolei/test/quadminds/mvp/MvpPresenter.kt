package com.leolei.test.quadminds.mvp

interface MvpPresenter<View : Any> {
    fun setView(view: View?)
}