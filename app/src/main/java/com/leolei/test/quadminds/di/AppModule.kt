package com.leolei.test.quadminds.di

import com.google.gson.Gson
import com.leolei.test.quadminds.notes.NotesPresenter
import com.leolei.test.quadminds.notes.NotesPresenterImpl
import com.leolei.test.quadminds.rest.NotesRestClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideNotesRestClient(baseUrl: String): NotesRestClient {
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        return retrofit.create(NotesRestClient::class.java)
    }

    @Provides
    @Singleton
    fun provideNotesPresenter(restClient: NotesRestClient): NotesPresenter {
        return NotesPresenterImpl(restClient)
    }
}