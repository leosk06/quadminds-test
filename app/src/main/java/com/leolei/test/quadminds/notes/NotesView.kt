package com.leolei.test.quadminds.notes

import com.leolei.test.quadminds.model.Note

interface NotesView {
    fun showNotes(notes: List<Note>?)
    fun showLoading(loading: Boolean)
    fun showError(errorMessage: String?)
}